﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Windows.Forms;
using GemBox.Spreadsheet;
using HtmlAgilityPack;

namespace cExcelConv
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String from = fromtxt.Text;
            String to = totxt.Text;
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xlsx",
                Filter = "excels files (*.xlsx)|*.xlsx",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

                var workbook = ExcelFile.Load(openFileDialog1.FileName);

                var worksheet = workbook.Worksheets[0];

                // Some of the properties from ExcelPrintOptions class are supported in HTML export.
                worksheet.PrintOptions.PrintHeadings = true;
                worksheet.PrintOptions.PrintGridlines = true;

                // Print area can be used to specify custom cell range which should be exported to HTML.
                worksheet.NamedRanges.SetPrintArea(worksheet.Cells.GetSubrange(from, to));

                var options = new HtmlSaveOptions()
                {
                    HtmlType = HtmlType.Html,
                    SelectionType = SelectionType.EntireFile,
                    FilesDirectoryPath = System.IO.Path.GetDirectoryName(openFileDialog1.FileName),
                };

                workbook.Save("HtmlExport.html", options);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog2 = new OpenFileDialog
            {
                InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "html",
                Filter = "html files (*.html)|*.html",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.Load(openFileDialog2.FileName);
                var body_text = htmlDocument.DocumentNode.SelectSingleNode("//body");
                var ascii = Encoding.Default.GetBytes(body_text.OuterHtml);
                string text = Encoding.UTF8.GetString(ascii);
                htmlBox.AppendText(text);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog3 = new OpenFileDialog
            {
                InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Title = "Browse Text Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "html",
                Filter = "html files (*.html)|*.html",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                FileInfo f = new FileInfo(openFileDialog2.FileName);
                f.CopyTo(Path.ChangeExtension(openFileDialog2.FileName, ".txt"));
            }
        }
    }
}
